# Pokédex Vue 3

Este proyecto es una Pokédex desarrollada con Vue 3 que obtiene datos de la API pública de Pokémon ([PokeAPI](https://pokeapi.co/)) y permite visualizar información sobre cada Pokémon. Además, implementa la funcionalidad de desplazamiento infinito para cargar más Pokémon a medida que el usuario se desplaza hacia abajo en la lista.

![Data available](./src/assets/screen2.png)
![No data](./src/assets/screen1.png)

## Características

- Visualización de una lista de Pokémon obtenidos de la API de PokeAPI.
- Detalles de cada Pokémon.
- Implementación de desplazamiento infinito para cargar más Pokémon a medida que el usuario se desplaza hacia abajo.

## Tecnologías Utilizadas

- Vue.js 3: Framework de JavaScript para construir interfaces de usuario.
- HTML5: Lenguaje de marcado para la estructura de la página web.
- CSS3: Lenguaje de estilos para el diseño y la presentación.
- JavaScript: Lenguaje de programación para la lógica de la aplicación.
- [PokeAPI](https://pokeapi.co/): API pública de Pokémon para obtener datos de los Pokémon.

## Instalación

## Clone project

```sh
git clone git@gitlab.com:dacost.dev/vue3-pokedex.git
```

### Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

## Licencia

Este proyecto está bajo la Licencia MIT. Para más detalles, consulta el archivo [LICENSE](LICENSE).
