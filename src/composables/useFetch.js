import { ref } from 'vue'

const useFetch = async (url, options = {}) => {
  const data = ref(null)
  const error = ref(null)
  const loading = ref(false)
  const { signal, abort } = new AbortController()

  loading.value = true
  try {
    const response = await fetch(url, { signal, ...options })

    if (!response.ok) {
      error.value = 'Error obtienendo los datos, intente mas tarde!'
    }
    const results = await response.json()
    data.value = results
  } catch (err) {
    error.value = err.message
  }
  loading.value = false

  return await {
    data: data.value,
    error: error.value,
    loading: loading.value,
    abort
  }
}

export default useFetch
