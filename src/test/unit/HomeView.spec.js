import { mount } from '@vue/test-utils'
import { describe, test, expect } from 'vitest'
import HomeView from '@/views/HomeView.vue'

describe('HomeView', () => {
  test('Montando vista', async () => {
    const wrapper = mount(HomeView)

    expect(wrapper).toBeTruthy()
  })

  test('makes a GET request to fetch todo list and returns the result', async () => {
    const todoListResponse = {
      count: 1302,
      next: 'https://pokeapi.co/api/v2/pokemon?offset=6&limit=6',
      previous: null,
      results: [
        { name: 'bulbasaur', url: 'https://pokeapi.co/api/v2/pokemon/1/' },
        { name: 'ivysaur', url: 'https://pokeapi.co/api/v2/pokemon/2/' },
        { name: 'venusaur', url: 'https://pokeapi.co/api/v2/pokemon/3/' },
        { name: 'charmander', url: 'https://pokeapi.co/api/v2/pokemon/4/' },
        { name: 'charmeleon', url: 'https://pokeapi.co/api/v2/pokemon/5/' },
        { name: 'charizard', url: 'https://pokeapi.co/api/v2/pokemon/6/' }
      ]
    }

    const data = await fetch('https://pokeapi.co/api/v2/pokemon?limit=6&offset=0').then(
      (response) => response.json()
    )

    expect(data).toBeTruthy(todoListResponse)
  })
})
